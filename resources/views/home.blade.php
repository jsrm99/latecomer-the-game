@extends('layouts.app')

@section('content')
    <div class="container">
        @if($customSuccess)
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>Yeet!</strong> {{ $customSuccess }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if($customError)
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>Holy guacamole!</strong> {{ $customError }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @include('partials.clock')
        @if($playDay)
            <div class="row justify-content-center mt-4">
                <div class="col-sm-4 text-center">
                    @if($isLatecomer)
                        <div class="guess-btn-wrapper"
                             @if(!$latecomer || $hasLogged) data-toggle="tooltip" data-placement="bottom"
                             title="{{ $latecomer ? 'Arrival already logged today.' : 'There is no latecomer yet.' }}" @endif>
                            <a href="{{ route('log-arrival') }}"
                               class="btn btn-block btn-dark{{ ($latecomer && !$hasLogged) ? '' : ' disabled'}}">
                                Log your arrival
                            </a>
                        </div>
                    @else
                        <div class="guess-btn-wrapper"
                             @if(!$latecomer || $hasGuessed || $hasLogged) data-toggle="tooltip" data-placement="bottom"
                             title="{{ $latecomer ? 'You have already guessed today or the latecomer already arrived.' : 'There is no latecomer yet.' }}" @endif>
                            <a href="{{ route('guess-arrival') }}"
                               class="btn btn-block btn-dark{{ (($latecomer && !$hasGuessed && !$hasLogged)) ? '' : ' disabled'}}">
                                Guess {{ $latecomer ? $latecomer->name : '?????' }}'s arrival
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        @endif
        <div class="row justify-content-center mt-5">
            <div class="col-sm-4 text-center">
                <h4 class="font-weight-bold">Scoreboard</h4>
                <hr class="mt-0 mb-3">
                @php $place = 0; @endphp
                @foreach($players as $player)
                    @php $place++; @endphp
                    <h5>
                        <b>
                            <small>{{ $place }}.</small>
                            {{ $player->name }}
                        </b>
                        <small>({{ $player->points }} pts.)</small>
                    </h5>
                @endforeach
            </div>
        </div>
    </div>
@endsection

@push('pageSpecificScripts')
    <script>
        console.log('Here, eat a quote <3: {{ $quote }}');
    </script>
@endpush