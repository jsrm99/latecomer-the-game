<div class="row">
    <div id="dashboardClock" class="col text-center">
        <h1 class="font-weight-bold mb-0 current-time" v-text="time"></h1>
        <h2 class="current-date" v-text="date"></h2>
        @if($latecomer)
            @if($playDay)
                @if(!$isLatecomer)
                    @if(!$hasLogged)
                        <h3>{{ $latecomer->name }} has not marked his arrival yet!</h3>
                    @else
                        <h3>{{ $latecomer->name }} arrived at {{ substr($hasLogged->time, 0, 5) }}!</h3>
                    @endif
                @else
                    @if(!$hasLogged)
                        <h3>Hi {{ $latecomer->name }}, did you arrive already?</h3>
                    @else
                        <h3>You arrived at {{ substr($hasLogged->time, 0, 5) }}!</h3>
                    @endif
                @endif
            @else
                <h3>We only play on Tuesday, Wednesday and Friday.</h3>
            @endif
        @else
            <h3>There is no latecomer yet...</h3>
        @endif
    </div>
</div>