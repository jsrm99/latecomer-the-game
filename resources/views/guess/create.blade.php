@extends('layouts.app')

@section('content')
    <div class="container">
        @include('partials.clock')
        <div class="row justify-content-center mt-4">
            <div class="col-sm-6">
                <div class="card bg-dark text-white text-center">
                    <h4 class="card-header">
                        Make a guess
                    </h4>
                    <div class="card-body">
                        <form action="{{ route('submit-guess') }}" method="POST">
                            @csrf
                            <div class="form-group mb-4">
                                <label for="time">What time do you think {{ $latecomer->name }} will arrive?</label>
                                <div class="form-row justify-content-center">
                                    <div class="col-7">
                                        <div class="input-group">
                                            <input id="time" name="time" type="time"
                                                   class="form-control bg-dark text-white @error('time') is-invalid @enderror"
                                                   value="{{ old('time') ? old('time') : '08:30' }}" required>
                                            <div class="input-group-append">
                                                <span class="input-group-text bg-dark text-white">uur</span>
                                            </div>
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <button type="submit" class="btn btn-block btn-dark btn-outline-success">Submit
                                    </button>
                                </div>
                                <div class="col">
                                    <a href="{{ route('home') }}" class="btn btn-block btn-dark btn-outline-danger">Cancel</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
