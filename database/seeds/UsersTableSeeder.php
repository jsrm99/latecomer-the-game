<?php

use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Exception
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'uuid' => Uuid::uuid4()->toString(),
                'name' => 'Juup',
                'email' => 'prive@juup.org',
                'password' => bcrypt('changeme'),
                'admin' => true,
                'latecomer' => false,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}
