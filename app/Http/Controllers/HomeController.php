<?php

namespace App\Http\Controllers;

use App\Arrival;
use App\Guess;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Inspiring;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the main page/dashboard
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $latecomer = User::where('latecomer', true)->first();
        $hasGuessed = Guess::whereDate('created_at', Carbon::today())->where('user_id', auth()->user()->id)->first() ? true : false;
        $todaysArrival = Arrival::whereDate('created_at', Carbon::today())->first();
        $hasLogged = $todaysArrival ? $todaysArrival : false;
        $playDays = [
            Carbon::TUESDAY,
            Carbon::WEDNESDAY,
            Carbon::FRIDAY,
        ];
        $today = new Carbon();
        $playDay = in_array($today->dayOfWeek, $playDays);

        return view('home')->with([
            'latecomer' => $latecomer,
            'isLatecomer' => ($latecomer ? auth()->user()->uuid === $latecomer->uuid : false),
            'hasGuessed' => $hasGuessed,
            'hasLogged' => $hasLogged,
            'playDay' => $playDay,
            'players' => User::orderByDesc('points')->get(),
            'quote' => Inspiring::quote(),
            'customError' => isset($_GET['error']) ? $_GET['error'] : null,
            'customSuccess' => isset($_GET['success']) ? $_GET['success'] : null,
        ]);
    }
}
