<?php

namespace App\Http\Controllers;

use App\Arrival;
use App\Guess;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GuessController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the make a guess form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $latecomer = User::where('latecomer', true)->first();

        if (Guess::whereDate('created_at', Carbon::today())->where('user_id', auth()->user()->id)->first()) {
            return redirect()->route('home', ['error' => 'You have already submitted your guess today.']);
        }
        if (!$latecomer) {
            return redirect()->route('home', ['error' => 'There is no latecomer yet.']);
        }
        if (auth()->user()->uuid === $latecomer->uuid) {
            return redirect()->route('home', ['error' => 'The latecomer can not guess.']);
        }
        $playDays = [
            Carbon::TUESDAY,
            Carbon::WEDNESDAY,
            Carbon::FRIDAY,
        ];
        $today = new Carbon();
        if (!in_array($today->dayOfWeek, $playDays)) {
            return redirect()->route('home', ['error' => 'We only play on Tuesday, Wednesday and Friday.']);
        }

        $todaysArrival = Arrival::whereDate('created_at', Carbon::today())->first();
        $hasLogged = $todaysArrival ? $todaysArrival : false;

        if ($hasLogged) {
            return redirect()->route('home', ['error' => 'The latecomer has already arrived.']);
        }

        return view('guess.create')->with([
            'latecomer' => $latecomer,
            'hasLogged' => $hasLogged,
            'playDay' => true,
            'isLatecomer' => ($latecomer ? auth()->user()->uuid === $latecomer->uuid : false),
        ]);
    }

    /**
     * Validate and store the guess
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $latecomer = User::where('latecomer', true)->first();

        if (Guess::whereDate('created_at', Carbon::today())->where('user_id', auth()->user()->id)->first()) {
            return redirect()->route('home', ['error' => 'You have already submitted your guess today.']);
        }
        if (!$latecomer) {
            return redirect()->route('home', ['error' => 'There is no latecomer yet.']);
        }
        if (auth()->user()->uuid === $latecomer->uuid) {
            return redirect()->route('home', ['error' => 'The latecomer can not guess.']);
        }
        $playDays = [
            Carbon::TUESDAY,
            Carbon::WEDNESDAY,
            Carbon::FRIDAY,
        ];
        $today = new Carbon();
        if (!in_array($today->dayOfWeek, $playDays)) {
            return redirect()->route('home', ['error' => 'We only play on Tuesday, Wednesday and Friday.']);
        }

        $request->validate([
            'time' => [
                'required',
                'date_format:h:i'
            ]
        ]);

        $guess = new Guess();
        $guess->time = $request->time;
        auth()->user()->guesses()->save($guess);

        return redirect()->route('home', ['success' => 'Guess has been saved, good luck!']);
    }
}
