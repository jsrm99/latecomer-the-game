<?php

namespace App\Http\Controllers;

use App\Arrival;
use App\Guess;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArrivalController extends Controller
{
    /**
     * HomeController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the make a arrival form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function create()
    {
        $latecomer = User::where('latecomer', true)->first();

        if (Arrival::whereDate('created_at', Carbon::today())->where('user_id', auth()->user()->id)->first()) {
            return redirect()->route('home', ['error' => 'You have already logged your arrival today.']);
        }
        if (!$latecomer) {
            return redirect()->route('home', ['error' => 'There is no latecomer yet.']);
        }
        if (auth()->user()->uuid !== $latecomer->uuid) {
            return redirect()->route('home', ['error' => 'Only the latecomer can log an arrival.']);
        }
        $playDays = [
            Carbon::TUESDAY,
            Carbon::WEDNESDAY,
            Carbon::FRIDAY,
        ];
        $today = new Carbon();
        if (!in_array($today->dayOfWeek, $playDays)) {
            return redirect()->route('home', ['error' => 'We only play on Tuesday, Wednesday and Friday.']);
        }

        $todaysArrival = Arrival::whereDate('created_at', Carbon::today())->first();
        $hasLogged = $todaysArrival ? $todaysArrival : false;

        return view('arrival.create')->with([
            'latecomer' => $latecomer,
            'hasLogged' => $hasLogged,
            'playDay' => true,
            'currTime' => Carbon::now()->format('H:i'),
            'isLatecomer' => ($latecomer ? auth()->user()->uuid === $latecomer->uuid : false),
        ]);
    }

    /**
     * Validate and store the arrival
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $latecomer = User::where('latecomer', true)->first();

        if (Arrival::whereDate('created_at', Carbon::today())->where('user_id', auth()->user()->id)->first()) {
            return redirect()->route('home', ['error' => 'You have already logged your arrival today.']);
        }
        if (!$latecomer) {
            return redirect()->route('home', ['error' => 'There is no latecomer yet.']);
        }
        if (auth()->user()->uuid !== $latecomer->uuid) {
            return redirect()->route('home', ['error' => 'Only the latecomer can log an arrival.']);
        }
        $playDays = [
            Carbon::TUESDAY,
            Carbon::WEDNESDAY,
            Carbon::FRIDAY,
        ];
        $today = new Carbon();
        if (!in_array($today->dayOfWeek, $playDays)) {
            return redirect()->route('home', ['error' => 'We only play on Tuesday, Wednesday and Friday.']);
        }

        $request->validate([
            'time' => [
                'required',
                'date_format:h:i'
            ]
        ]);

        $arrival = new Arrival();
        $arrival->time = $request->time;
        auth()->user()->arrivals()->save($arrival);

        $correctGuesses = Guess::whereDate('created_at', Carbon::today())->where('time', $arrival->time)->get();
        $earlyGuesses = Guess::whereDate('created_at', Carbon::today())->where('time', Carbon::parse($arrival->time)->subMinute())->get();
        $lateGuesses = Guess::whereDate('created_at', Carbon::today())->where('time', Carbon::parse($arrival->time)->addMinute())->get();
        if (count($correctGuesses) || count($earlyGuesses) || count($lateGuesses)) {
            if (count($correctGuesses)) {
                foreach ($correctGuesses as $correctGuess) {
                    $correctGuess->user->points += 2;
                    $correctGuess->user->save();
                }
            }
            if (count($earlyGuesses)) {
                foreach ($earlyGuesses as $earlyGuess) {
                    $earlyGuess->user->points++;
                    $earlyGuess->user->save();
                }
            }
            if (count($lateGuesses)) {
                foreach ($lateGuesses as $lateGuess) {
                    $lateGuess->user->points++;
                    $lateGuess->user->save();
                }
            }
            return redirect()->route('home', ['success' => 'Arrival has been saved, someone guessed it correctly!']);
        } else {
            auth()->user()->points++;
            auth()->user()->save();
            return redirect()->route('home', ['success' => 'Arrival has been saved, no one guessed it correctly!']);
        }
    }
}
