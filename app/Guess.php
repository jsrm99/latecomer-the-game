<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Guess extends Model
{
    use Uuids, SoftDeletes;

    /**
     * Get the user the guess belongs to
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
