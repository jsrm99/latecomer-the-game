<?php

namespace App;

use App\Traits\Uuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Arrival extends Model
{
    use Uuids, SoftDeletes;

    /**
     * Get the user the arrival belongs to
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
