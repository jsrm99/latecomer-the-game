<?php

namespace App\Traits;

use Ramsey\Uuid\Uuid;

trait Uuids
{
    /**
     *  Overwrite original boot function to use uuid's
     */
    public static function boot()
    {
        parent::boot();
        self::creating(function ($model) {
            $model->uuid = Uuid::uuid4()->toString();
        });
    }

}