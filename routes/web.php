<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/log-arrival', 'ArrivalController@create')->name('log-arrival');
Route::post('/submit-arrival', 'ArrivalController@store')->name('submit-arrival');

Route::get('/make-a-guess', 'GuessController@create')->name('guess-arrival');
Route::post('/submit-guess', 'GuessController@store')->name('submit-guess');